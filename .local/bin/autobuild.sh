#!/bin/bash
# Copyright (C) 2014, 2016-2017 Luke Shumaker <lukeshu@sbcglobal.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

export PATH # because of setuid safety, path may be currently un-exported

withtty() {
	local cmd
	printf -v cmd '%q ' "$@"
	script --return --quiet --command "$cmd" /dev/null
}

usage() {
	print "Usage: %q REPO/PKGBASE [FILES...]" "${0##*/}"
	print "When run from a git directory, automatically updates the package based on FILES in the git repo"
}

main() {
	set -euE
	. "$(librelib messages)"
	setup_traps

	# Get the date as the *very* first thing.
	#
	# We get the current date instead of getting a date from git
	# because git time is non-monotonic.  (I mean, the system time
	# is also non-monotonic.  But at day-granularity, I don't
	# expect that to ever come up.)  A git-rebase or a weird merge
	# could easily break things if we get the git time.
	newpkgver_date=$(LC_ALL=C date -u +%Y%m%d)

	# Configuration parsing ################################################

	. "$(librelib conf)"
	if type load_conf &>/dev/null; then
		# new way
		load_conf libretools.conf WORKDIR ABSLIBRERECV ABSLIBRESEND
	else
		# old way
		load_files libretools
		check_vars libretools WORKDIR ABSLIBRERECV ABSLIBRESEND
	fi

	# Lock to pevent conflicting runs ######################################
	lock 9 "${WORKDIR}/autobuilder.lock" "Waiting for previous run of %q to finish" "$0"

	# Option/usage parsing #################################################

	if [[ $# -lt 1 ]]; then
		error "%q takes at least 1 argument" "$0"
		usage >&2
		return 1
	fi

	package=$1
	package_re='^[^/]+/[^/]+$'
	if ! [[ $package =~ $package_re ]]; then
		error "The first argument must be in the format REPO/PKGBASE: %s" "$package"
		usage >&2
		return 1
	fi
	unset package_re

	if [[ $PWD != *.git ]]; then
		die "%q should be run as a hook from a git repository" "$0"
	fi

	newgitver=$(git log -n1 --format='%H' master -- "${@:2}")

	# The real work begins! ################################################

	# Get the ABSLibre tree
	cd "$WORKDIR/abslibre"
	env -i git reset --hard origin/master
	gitget -f -p "$ABSLIBRESEND" checkout "$ABSLIBRERECV" "$WORKDIR/abslibre"
	if ! [[ -f "${WORKDIR}/abslibre/${package}/PKGBUILD" ]]; then
		die "package does not exist in abslibre.git: %s" "$package"
	fi
	cd "$WORKDIR/abslibre/${package}"

	# Figure out info about the last version
	oldgitver=$(sed -n 's/^_gitver=//p' PKGBUILD)
	oldpkgver=$(sed -n 's/^pkgver=//p' PKGBUILD)
	oldpkgver_date=${oldpkgver%%.*}
	oldpkgver_rel=${oldpkgver#${oldpkgver_date}}; oldpkgver_rel=${oldpkgver_rel#.}; oldpkgver_rel=${oldpkgver_rel:-0}

	# Make sure we actually have changes
	if [[ "$newgitver" == "$oldgitver" ]]; then
		msg 'No new changes were committed, nothing to do'
		return 0
	fi

	# Handle doing multiple versions in the same day
	if [[ "$newpkgver_date" == "$oldpkgver_date" ]]; then
		declare -i newpkgver_rel=${oldpkgver_rel}+1
		newpkgver=${newpkgver_date}.${newpkgver_rel}
	else
		newpkgver=${newpkgver_date}
	fi

	# Update the PKGBUILD
	sed -i -e "s|^pkgver=.*|pkgver=${newpkgver}|" \
		-e "s|^_gitver=.*|_gitver=${newgitver}|" \
		-e 's|^pkgrel=.*|pkgrel=1|' \
		PKGBUILD
	updpkgsums
	git add PKGBUILD
	git commit -m "Update ${package}"

	# Build the new package
	withtty sudo librechroot -n autobuilder -l root update
	withtty sudo librechroot -n autobuilder sync
	withtty sudo libremakepkg -n autobuilder
	librestage

	# Publish the updates
	git push
	librerelease
}

main "$@"
