/* Just a "stupid", secure SUID wrapper around autobuild.sh */
/* Copyright (C) 2014 Luke Shumaker <lukeshu@sbcglobal.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE /* for clearenv(3) */
#include <alloca.h> /* for alloca(3) */
#include <errno.h>  /* for errno */
#include <error.h>  /* for error(3) (GNU extension) */
#include <pwd.h>    /* for getpwuid(3) */
#include <stdlib.h> /* for getenv(3), setenv(3), clearenv(3) */
#include <string.h> /* for strlen(3), strcpy(3) */
#include <unistd.h> /* for geteuid(3), setreuid(3), execv(3) */

void
mysetenv(const char *name, const char *value)
{
	if (value != NULL) {
		if (setenv(name, value, 1) != 0) {
			error(127, errno, "could not set %s", name);
		}
	}
}

int
main(int argc, char **argv)
{
	errno = 0;
	struct passwd *user = getpwuid(geteuid());
	if (!user)
		error(127, errno, "could not look up effective user (%ld) info", (long)geteuid());

	if (setreuid(geteuid(), -1) < 0)
		error(127, errno, "could not set real UID to match effective UID (%ld)", (long)geteuid());

	const char *env_term        = getenv("TERM");
	const char *env_lang        = getenv("LANG");
	const char *env_lc_all      = getenv("LC_ALL");
	const char *env_lc_collate  = getenv("LC_COLLATE");
	const char *env_lc_ctype    = getenv("LC_CTIME");
	const char *env_lc_messages = getenv("LC_MESSAGES");
	const char *env_lc_monetary = getenv("LC_MONETARY");
	const char *env_lc_numeric  = getenv("LC_NUMERIC");
	const char *env_lc_time     = getenv("LC_TIME");
	clearenv();
	mysetenv("USER"       , user->pw_name  );
	mysetenv("LOGNAME"    , user->pw_name  );
	mysetenv("HOME"       , user->pw_dir   );
	mysetenv("TERM"       , env_term       );
	mysetenv("LANG"       , env_lang       );
	mysetenv("LC_ALL"     , env_lc_all     );
	mysetenv("LC_COLLATE" , env_lc_collate );
	mysetenv("LC_CTIME"   , env_lc_ctype   );
	mysetenv("LC_MESSAGES", env_lc_messages);
	mysetenv("LC_MONETARY", env_lc_monetary);
	mysetenv("LC_NUMERIC" , env_lc_numeric );
	mysetenv("LC_TIME"    , env_lc_time    );

	const char *script_suffix = "/.local/bin/autobuild.sh";
	char *script = alloca(strlen(user->pw_dir)+strlen(script_suffix)+1);
	strcpy(script, user->pw_dir);
	strcpy(&(script[strlen(user->pw_dir)]), script_suffix);

	execv(script, argv);
	error(127, errno, "%s", script);
}
